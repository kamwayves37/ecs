package yves.kams.game.fx.core.ecs.entities

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import yves.kams.game.fx.core.ecs.components.Component
import yves.kams.game.fx.core.ecs.world.World
import yves.kams.game.fx.core.settings.GameSetting

internal class EntityTest {
    private val manager: EntityManager = EntityManager(World(GameSetting()))
    private lateinit var entity: Entity

    @BeforeEach
    fun setUp() {
        entity = manager.createEntity(TypeTest.PLAYER)
        entity.addComponent(ComponentA(0.3))
    }

    @Test
    fun getId() {
        assertTrue(entity.id >= 0)
    }

    @Test
    fun getType() {
        assertEquals(TypeTest.PLAYER, entity.type)
    }

    @Test
    fun setType() {
        entity.type = TypeTest.ENEMY

        assertEquals(TypeTest.ENEMY, entity.type)
    }

    @Test
    fun addComponent() {
        entity.addComponent(ComponentB(true))

        assertTrue(entity.hasComponent(ComponentB::class))
        assertFalse(entity.hasComponent(ComponentC::class))
    }

    @Test
    fun getComponent() {
        val a: ComponentA = entity.getComponent(ComponentA::class)

        assertEquals(0.3, a.speed)
    }

    @Test
    fun hasComponent() {
        assertTrue(entity.hasComponent(ComponentA::class))
    }

    @Test
    fun removeComponent() {
        entity.removeComponent(ComponentA::class)
        assertFalse(entity.hasComponent(ComponentA::class))
    }

    @Test
    fun removeAllComponent() {
        entity.addComponent(ComponentB(true))

        entity.removeAllComponent()

        assertFalse(entity.hasComponent(ComponentB::class))
        assertFalse(entity.hasComponent(ComponentA::class))
    }

    @Test
    fun typesComponent() {
        val types = entity.typesComponent()

        assertTrue(types?.contains(ComponentA::class) ?: false)
    }

    internal enum class TypeTest{
        PLAYER,
        ENEMY
    }

    private data class ComponentA(val speed: Double) : Component()
    private data class ComponentB(val alive: Boolean) : Component()
    private class ComponentC : Component()
}