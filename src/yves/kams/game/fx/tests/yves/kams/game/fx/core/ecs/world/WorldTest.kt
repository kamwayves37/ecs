package yves.kams.game.fx.core.ecs.world

import javafx.scene.input.KeyCode
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import yves.kams.game.fx.core.ecs.systems.System
import yves.kams.game.fx.core.settings.GameSetting

internal class WorldTest {
    private lateinit var world: World

    @BeforeEach
    fun setUp() {
        world = World(GameSetting())
                .registerSystem(SystemA::class)
    }

    @Test
    fun registerSystem() {
        world.registerSystem(SystemB::class)
        assertTrue(world.hasSystem(SystemB::class))
    }

    @Test
    fun getSystem() {
        world.onInit()
        val sys = world.getSystem(SystemA::class)
        assertEquals(1997, sys?.number)
    }

    @Test
    fun createEntity() {
        assertNotNull(world.createEntity(KeyCode.U))
    }

    internal class SystemA: System(){
        var number: Int = 1997
        override fun onInit() {
            println("Init A")
        }
    }
    internal class SystemB: System(){
        val test: Boolean = true

        override fun onInit() {
            println("Init B")
        }
    }
    internal class SystemC: System(){
        override fun onInit() {
            println("Init C")
        }
    }
}