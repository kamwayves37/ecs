package yves.kams.game.fx.core.event

import javafx.event.Event
import javafx.event.EventHandler
import javafx.event.EventType
import javafx.scene.input.KeyEvent
import javafx.scene.input.ZoomEvent
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class EventBusTest {
    private lateinit var subscribe: Subscribe
    private var value: Boolean = false


    private fun testHandler(event: TestEvent){
        if(event.eventType === TestEvent.ANY)
            value = true
    }

    @BeforeEach
    fun setUp() {
        EventBus.INSTANCE.addEventHandler(TestEvent.ANY, EventHandler {testHandler(it)})
    }

    @Test
    internal fun fireEvent() {
        EventBus.INSTANCE.fireEvent(TestEvent(TestEvent.ANY, value))
        assertTrue(value)
    }


    internal class TestEvent(type: EventType<TestEvent>, value: Boolean): Event(type){
        companion object{
            val ANY = EventType<TestEvent>(Event.ANY, "TEST EVENT")
        }
    }
}