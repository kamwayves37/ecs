package yves.kams.game.fx.core.ecs.query

import javafx.event.EventHandler
import yves.kams.game.fx.core.ecs.components.ComponentEvent
import yves.kams.game.fx.core.ecs.entities.EntityEvent
import yves.kams.game.fx.core.event.EventBus
import yves.kams.game.fx.core.event.Subscribe


class QueryManager {
    private val queries: MutableList<Query> = mutableListOf()
    private var subscribeEntity: Subscribe
    private var subscribeComponent: Subscribe

    init {
        subscribeEntity = EventBus.INSTANCE.addEventHandler(EntityEvent.ENTITY_REMOVED, EventHandler { onEntityRemoved(it) })
        subscribeComponent = EventBus.INSTANCE.addEventHandler(ComponentEvent.ANY, EventHandler {
            when(it.eventType){
                ComponentEvent.COMPONENT_ADDED -> onComponentAdded(it)
                ComponentEvent.COMPONENT_REMOVED -> onComponentRemoved(it)
            }
        })
    }

    fun register(query: Query) {
        queries.add(query)
    }

    private fun onEntityRemoved(event: EntityEvent) {
        queries.forEach {
            if (it.hasEntity(event.entity) && !it.hasEntityRemoved(event.entity))
                it.removeEntity(event.entity)
        }
    }

    private fun onComponentAdded(event: ComponentEvent) {
        queries.forEach {
            if (it.match(event.entity.typesComponent()) &&
                    !it.hasEntity(event.entity) &&
                    !it.hasEntityAdded(event.entity))
                it.addEntity(event.entity)
        }
    }

    private fun onComponentRemoved(event: ComponentEvent) {
        queries.forEach {
            if (it.match(event.component::class) &&
                    it.hasEntity(event.entity) &&
                    !it.hasEntityRemoved(event.entity))
                it.removeEntity(event.entity)
        }
    }

    fun destroy(){
        queries.forEach { it.reset() }
        subscribeEntity.unSubscribe()
        subscribeComponent.unSubscribe()
    }
}