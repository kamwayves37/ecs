package yves.kams.game.fx.core.ecs.query

import yves.kams.game.fx.core.ecs.components.Component
import yves.kams.game.fx.core.ecs.entities.Entity
import java.util.function.Consumer
import kotlin.reflect.KClass

typealias Query = QueryModel

open class QueryModel {
    var key: List<KClass<out Component>> = listOf()
    var onEntityAdded: Consumer<Entity>? = null
    var onEntityRemoved: Consumer<Entity>? = null

    private val entities: MutableList<Entity> = mutableListOf()
    private val entitiesAdded: MutableList<Entity> = mutableListOf()
    private val entitiesRemoved: MutableList<Entity> = mutableListOf()

    private var onAdded = false
    private var onRemoved = false

    val results: List<Entity>
        get() {
            if (onAdded) {
                entities.addAll(entitiesAdded)
                entitiesAdded.clear()
                onAdded = false
            }

            if (onRemoved) {
                entities.removeAll(entitiesRemoved)
                entitiesRemoved.clear()
                onRemoved = false
            }

            return entities
        }

    fun match(types: Set<KClass<out Component>>): Boolean {
        return types.containsAll(key)
    }

    fun match(type: KClass<out Component>): Boolean {
        return key.contains(type)
    }

    fun hasEntity(entity: Entity): Boolean {
        return entities.contains(entity)
    }

    fun hasEntityAdded(entity: Entity): Boolean {
        return entitiesAdded.contains(entity)
    }

    fun hasEntityRemoved(entity: Entity): Boolean {
        return entitiesRemoved.contains(entity)
    }

    fun removeEntity(entity: Entity) {
        onRemoved = entitiesRemoved.add(entity)
        onEntityRemoved?.accept(entity)
    }

    fun addEntity(entity: Entity) {
        onAdded = entitiesAdded.add(entity)
        onEntityAdded?.accept(entity)
    }

    fun reset() {
        onRemoved = false
        onAdded = false
        entities.clear()
        entitiesRemoved.clear()
        entitiesAdded.clear()
    }
}