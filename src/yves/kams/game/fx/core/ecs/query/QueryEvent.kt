package yves.kams.game.fx.core.ecs.query

import javafx.event.Event
import javafx.event.EventType

class QueryEvent(eventType: EventType<QueryEvent>) : Event(eventType){
    companion object{
        val ANY = EventType<QueryEvent>(Event.ANY, "QUERY EVENT")
    }
}