package yves.kams.game.fx.core.ecs.world

import yves.kams.game.fx.core.ecs.entities.Entity
import yves.kams.game.fx.core.ecs.entities.EntityManager
import yves.kams.game.fx.core.ecs.systems.System
import yves.kams.game.fx.core.ecs.systems.SystemManager
import yves.kams.game.fx.core.settings.GameSetting
import yves.kams.game.fx.core.services.GameService
import kotlin.reflect.KClass

class World(val setting: GameSetting): GameService() {
    private val entityManager: EntityManager = EntityManager(this)
    private val systemManager: SystemManager = SystemManager(this)


    fun <T : System> registerSystem(type: KClass<T>) : World {
        systemManager.register(type)
        return this
    }

    fun <T : System> getSystem(type: KClass<T>): T? {
        return systemManager[type]
    }

    fun <T : System> hasSystem(type: KClass<T>): Boolean{
        return systemManager.has(type)
    }

    fun createEntity(type: Enum<*>): Entity {
        return entityManager.createEntity(type)
    }

    fun destroyEntity(entity: Entity){
        entityManager.removeEntity(entity)
    }

    override fun onInit(){
        systemManager.onInit()
    }

    override fun onUpdate(dt: Double){
        systemManager.onUpdate(dt)
    }

    override fun onExit(){
        systemManager.onExit()
    }
}