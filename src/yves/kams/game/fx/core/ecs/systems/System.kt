package yves.kams.game.fx.core.ecs.systems

import yves.kams.game.fx.core.ecs.world.World
import yves.kams.game.fx.core.ecs.query.Query

abstract class System {
    val query: Query = Query()
    lateinit var world: World

    open fun onInit() {}

    open fun onUpdate(dt: Double) {}

    open fun onExit() {}
}