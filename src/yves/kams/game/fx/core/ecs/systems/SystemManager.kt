package yves.kams.game.fx.core.ecs.systems

import yves.kams.game.fx.core.ecs.query.QueryManager
import yves.kams.game.fx.core.ecs.world.World
import yves.kams.game.fx.core.utils.Reflection
import kotlin.reflect.KClass
import kotlin.reflect.cast

class SystemManager(private val world: World) {
    private val systemsCache: MutableMap<KClass<out System>, System?> = mutableMapOf()
    private val systems: MutableList<System> = mutableListOf()
    private val queryManager: QueryManager = QueryManager()

    fun <T : System> register(type: KClass<T>) {
        systemsCache[type] = null
    }

    operator fun <T : System> get(type: KClass<T>): T? {
        val system = systemsCache[type]

        if (system !== null)
            return type.cast(system)

        throw IllegalAccessError("Error accessing the system " + type.simpleName + " because it is absent or not initialized")
    }

    fun types(): Set<KClass<out System>> {
        return systemsCache.keys
    }

    fun <T : System> has(type: KClass<T>): Boolean{
        return systemsCache.containsKey(type)
    }

    fun onInit() {
        for (type in types()) {
            val system = Reflection.newInstance(type.java)
            system.world = world
            system.onInit()

            queryManager.register(system.query)

            systemsCache[type] = system
            systems.add(system)
        }
    }

    fun onUpdate(dt: Double) {
        systems.forEach { it.onUpdate(dt) }
    }

    fun onExit() {
        systems.forEach { it.onExit() }
    }
}