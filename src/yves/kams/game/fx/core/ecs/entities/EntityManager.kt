package yves.kams.game.fx.core.ecs.entities

import yves.kams.game.fx.core.ecs.components.Component
import yves.kams.game.fx.core.ecs.components.ComponentEvent
import yves.kams.game.fx.core.ecs.world.World
import yves.kams.game.fx.core.event.EventBus
import yves.kams.game.fx.core.generic.GenericMap
import kotlin.reflect.KClass
import kotlin.reflect.cast

private typealias ComponentManager = GenericMap<KClass<out Component>, Component>

class EntityManager(private val world: World) {
    private val entities: HashMap<Entity, ComponentManager> = HashMap()

    fun createEntity(type: Enum<*>): Entity {
        val entity = Entity(this)

        entity.type = type
        entity.world = world

        entities[entity] = ComponentManager()

        EventBus.INSTANCE.fireEvent(EntityEvent(EntityEvent.ENTITY_CREATED, entity))

        return entity
    }

    fun removeEntity(entity: Entity) {
        val manager = entities.remove(entity)
        manager?.clear()

        EventBus.INSTANCE.fireEvent(EntityEvent(EntityEvent.ENTITY_REMOVED, entity))
    }

    fun count(entity: Entity): Int {
        return entities[entity]?.size ?: 0
    }

    operator fun set(entity: Entity, component: Component) {
        entities[entity]?.set(component::class, component)

        EventBus.INSTANCE.fireEvent(ComponentEvent(ComponentEvent.COMPONENT_ADDED, entity, component))
    }

    operator fun <T : Component> get(entity: Entity, type: KClass<T>): T {
        val component = entities[entity]?.get(type);

        if (component !== null)
            return type.cast(component)

        throw IllegalAccessError("Error accessing component " + type.simpleName + " in $entity")
    }

    fun <T : Component> has(entity: Entity, type: KClass<T>): Boolean? {
        return entities[entity]?.has(type)
    }

    fun <T : Component> remove(entity: Entity, type: KClass<T>): Boolean {
        val component = entities[entity]?.remove(type)

        if (component !== null)
            EventBus.INSTANCE.fireEvent(ComponentEvent(ComponentEvent.COMPONENT_REMOVED, entity, component))

        return component !== null
    }

    fun removeAll(entity: Entity) {
        entities[entity]?.clear()
    }

    fun types(entity: Entity): Set<KClass<out Component>> {
        return entities[entity]?.types() ?: throw IllegalAccessError("Error accessing types component in $entity")
    }
}