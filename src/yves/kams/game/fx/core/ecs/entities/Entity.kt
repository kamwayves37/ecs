package yves.kams.game.fx.core.ecs.entities

import yves.kams.game.fx.core.ecs.components.Component
import yves.kams.game.fx.core.ecs.world.World
import  yves.kams.game.fx.core.utils.Generate
import kotlin.reflect.KClass

class Entity(private val manager: EntityManager) {
    val id: Int = Generate.ID()
    lateinit var type: Enum<*>
    lateinit var world: World

    fun removeToWorld() {
        manager.removeEntity(this)
    }

    fun addComponent(component: Component): Entity {
        manager[this] = component
        return this
    }

    fun <T : Component> getComponent(type: KClass<T>): T {
        return manager[this, type]
    }

    fun <T : Component> hasComponent(type: KClass<T>): Boolean {
        return manager.has(this, type) ?: false
    }

    fun <T : Component> removeComponent(type: KClass<T>) {
        manager.remove(this, type)
    }

    fun removeAllComponent() {
        manager.removeAll(this)
    }

    fun typesComponent(): Set<KClass<out Component>> {
        return manager.types(this)
    }

    fun componentCount(): Int {
        return manager.count(this)
    }

    override fun toString(): String {
        return "Entity{Id: ".plus(id).plus(", Type: ").plus(type.name).plus("}")
    }
}