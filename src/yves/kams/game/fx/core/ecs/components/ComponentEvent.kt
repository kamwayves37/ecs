package yves.kams.game.fx.core.ecs.components

import javafx.event.Event
import javafx.event.EventType
import yves.kams.game.fx.core.ecs.entities.Entity

class ComponentEvent(eventType: EventType<ComponentEvent>, val entity: Entity, val component: Component) : Event(eventType) {
    companion object {
        val ANY = EventType<ComponentEvent>(Event.ANY, "COMPONENT EVENT")
        val COMPONENT_ADDED = EventType(ANY, "COMPONENT ADDED")
        val COMPONENT_REMOVED = EventType(ANY, "COMPONENT REMOVE")
    }
}