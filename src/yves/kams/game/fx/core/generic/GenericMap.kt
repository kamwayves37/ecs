package yves.kams.game.fx.core.generic

import java.util.*
import kotlin.collections.HashMap

open class GenericMap<T, V> : Generic<T, V>{
    private val map: HashMap<T, V> = hashMapOf()

    override val size: Int
        get() = map.size

    override fun get(key: T): V? {
        return map[key]
    }

    override fun getOptional(key: T): Optional<V> {
        return Optional.ofNullable(this[key])
    }

    override fun set(key: T, value: V) {
        map[key] = value
    }

    override fun remove(key: T): V? {
        return map.remove(key)
    }

    override fun clear() {
        map.clear()
    }

    override fun has(key: T): Boolean {
        return map.containsKey(key)
    }

    override fun types(): Set<T> {
        return map.keys
    }
}