package yves.kams.game.fx.core.generic

import java.util.*

interface Generic<T, V> {
    val size: Int

    operator fun get(key: T): V?

    fun getOptional(key: T): Optional<V>

    operator fun set(key: T, value: V)

    fun remove(key: T): V?

    fun clear()

    fun has(key: T): Boolean

    fun types(): Set<T>
}