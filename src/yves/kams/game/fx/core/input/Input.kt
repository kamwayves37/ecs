package yves.kams.game.fx.core.input

import javafx.scene.input.KeyCode
import javafx.scene.input.MouseButton
import yves.kams.game.fx.core.cache.CacheType
import yves.kams.game.fx.core.services.GameService
import yves.kams.game.fx.core.input.keyboard.Keyboard
import yves.kams.game.fx.core.input.keyboard.KeyboardState
import yves.kams.game.fx.core.input.mouse.Mouse
import yves.kams.game.fx.core.input.mouse.MouseState
import yves.kams.game.fx.core.settings.ReadOnlyGameSetting

internal class Input(private val setting: ReadOnlyGameSetting) : GameService(), KeyboardState, MouseState, InputActionListener {
    private val keyboard: Keyboard = Keyboard(setting)
    private val keyStates: MutableMap<KeyCode, InputButton> = mutableMapOf()
    private val keyboardAction: MutableMap<KeyCode, InputAction> = mutableMapOf()

    private val mouse: Mouse = Mouse(setting)
    private val mouseStates: MutableMap<MouseButton, InputButton> = mutableMapOf()
    private val mouseAction: MutableMap<MouseButton, InputAction> = mutableMapOf()

    override val mouseX: Double
        get() = mouse.sceneX

    override val mouseY: Double
        get() = mouse.sceneY

    override fun onInit() {
        TODO("Init Default input action")
    }

    override fun addKeyboardAction(key: KeyCode, action: InputAction): InputActionListener {
        keyboardAction[key] = action
        return this
    }

    override fun addMouseAction(key: MouseButton, action: InputAction): InputActionListener {
        mouseAction[key] = action
        return this
    }

    override fun onUpdate(dt: Double) {
        keyboard.current.forEach { (key, status) ->
            val button = getKeyButton(key)

            button.reset()

            if (status) {
                val state = keyboard.getKey(key)
                val action = keyboardAction[key]

                if (!state.prev && state.current) {
                    action?.onPressed()
                    button.keyDown = true
                }

                if (state.prev && state.current) {
                    action?.onPressedDown()
                    button.keyPressed = true
                }

                if (!state.current && state.prev) {
                    action?.onReleased()
                    keyboard[key] = false
                    button.keyUp = true
                }

                state.prev = state.current
            } else {
                setting.cache[Runnable {
                    keyboard.current.remove(key)
                }] = CacheType.REMOVED
            }
        }

        mouse.current.forEach { (button, status) ->
            val mouseButton = getMouseButton(button)

            mouseButton.reset()

            if (status) {
                val state = mouse.getKey(button)
                val action = mouseAction[button]

                if (!state.prev && state.current) {
                    action?.onPressed()
                    mouseButton.keyDown = true
                }

                if (state.prev && state.current) {
                    action?.onPressedDown()
                    mouseButton.keyPressed = true
                }

                if (!state.current && state.prev) {
                    action?.onReleased()
                    mouse[button] = false
                    mouseButton.keyUp = true
                }

                state.prev = state.current
            } else {
                setting.cache[Runnable {
                    mouse.current.remove(button)
                }] = CacheType.REMOVED
            }
        }
    }

    private fun getKeyButton(key: KeyCode): InputButton {
        var inputButton = keyStates[key]

        if (inputButton === null) {
            inputButton = InputButton()
            keyStates[key] = inputButton
        }

        return inputButton
    }

    private fun getMouseButton(key: MouseButton): InputButton {
        var inputButton = mouseStates[key]

        if (inputButton === null) {
            inputButton = InputButton()
            mouseStates[key] = inputButton
        }

        return inputButton
    }

    override fun isKeyPressed(key: KeyCode): Boolean {
        return keyStates[key]?.keyPressed ?: false
    }

    override fun isKeyDown(key: KeyCode): Boolean {
        return keyStates[key]?.keyDown ?: false
    }

    override fun isKeyUp(key: KeyCode): Boolean {
        return keyStates[key]?.keyUp ?: false
    }

    override fun mouseDown(button: MouseButton): Boolean {
        return mouseStates[button]?.keyDown ?: false
    }

    override fun mouseClicked(button: MouseButton): Boolean {
        return mouseStates[button]?.keyPressed ?: false
    }

    override fun mouseUp(button: MouseButton): Boolean {
        return mouseStates[button]?.keyUp ?: false
    }

    override fun onExit() {
    }
}