package yves.kams.game.fx.core.input

data class InputButton(var keyPressed: Boolean = false,
                       var keyDown: Boolean = false,
                       var keyUp: Boolean = false){
    fun reset(){
        keyPressed = false
        keyDown = false
        keyUp = false
    }
}