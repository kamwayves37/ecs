package yves.kams.game.fx.core.input

import javafx.scene.input.KeyCode
import javafx.scene.input.MouseButton

interface InputActionListener {
    fun addKeyboardAction(key: KeyCode, action: InputAction): InputActionListener
    fun addMouseAction(key: MouseButton, action: InputAction): InputActionListener
}