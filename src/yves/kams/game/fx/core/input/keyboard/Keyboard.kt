package yves.kams.game.fx.core.input.keyboard

import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import yves.kams.game.fx.core.input.State
import yves.kams.game.fx.core.settings.ReadOnlyGameSetting

class Keyboard(setting: ReadOnlyGameSetting) {
    private val mapping: MutableMap<KeyCode, State> = mutableMapOf()
    val current: MutableMap<KeyCode, Boolean> = mutableMapOf()

    operator fun set(key: KeyCode, status: Boolean){
        current[key] = status
    }

    fun remove(key: KeyCode){
        current.remove(key)
    }

    init {
        setting.scene.addEventFilter(KeyEvent.KEY_PRESSED) { event ->
            setKey(event.code, true)
            current[event.code] = true
        }
        setting.scene.addEventFilter(KeyEvent.KEY_RELEASED) { event -> setKey(event.code, false) }
    }

    fun getKey(key: KeyCode): State {
        var state = mapping[key]

        if (state === null) {
            state = State()
            mapping[key] = state
        }

        return state
    }

    private fun setKey(key: KeyCode, status: Boolean) {
        val state = getKey(key)
        state.prev = state.current
        state.current = status
    }
}