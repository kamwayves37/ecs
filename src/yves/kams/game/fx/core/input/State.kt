package yves.kams.game.fx.core.input

data class State(var prev: Boolean = false, var current: Boolean = false)