package yves.kams.game.fx.core.input.mouse

import javafx.scene.input.MouseButton
import javafx.scene.input.MouseEvent
import yves.kams.game.fx.core.input.State
import yves.kams.game.fx.core.settings.GameSetting
import yves.kams.game.fx.core.settings.ReadOnlyGameSetting

class Mouse(setting: ReadOnlyGameSetting) {
    private val mapping: MutableMap<MouseButton, State> = mutableMapOf()
    val current: MutableMap<MouseButton, Boolean> = mutableMapOf()

    var sceneX: Double = 0.0
    var sceneY: Double = 0.0

    operator fun set(button: MouseButton, value: Boolean) {
        current[button] = value
    }

    init {
        setting.scene.addEventFilter(MouseEvent.ANY) { event ->

            when (event.eventType) {
                MouseEvent.MOUSE_PRESSED -> {
                    setKey(event.button, true)
                    current[event.button] = true
                }

                MouseEvent.MOUSE_RELEASED -> {
                    setKey(event.button, false)
                }

                MouseEvent.MOUSE_MOVED -> {
                    sceneX = event.sceneX
                    sceneY = event.sceneY
                }

                MouseEvent.MOUSE_DRAGGED -> {
                    sceneX = event.sceneX
                    sceneY = event.sceneY
                }
            }
        }
    }

    fun getKey(key: MouseButton): State {
        var state = mapping[key]

        if (state === null) {
            state = State()
            mapping[key] = state
        }

        return state
    }

    private fun setKey(key: MouseButton, status: Boolean) {
        val state = getKey(key)
        state.prev = state.current
        state.current = status
    }
}