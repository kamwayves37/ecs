package yves.kams.game.fx.core.input.mouse

import javafx.scene.input.MouseButton

interface MouseState {
    val mouseX: Double
    val mouseY: Double

    fun mouseDown(button: MouseButton): Boolean = false

    fun mouseClicked(button: MouseButton): Boolean = false

    fun mouseUp(button: MouseButton): Boolean = false
}