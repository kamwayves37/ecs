package yves.kams.game.fx.core.input

 open class InputAction {

    fun onPressed() { pressed() }

    fun onPressedDown() { pressedDown() }

    fun onReleased() { released() }

    protected open fun pressed(){}

    protected open fun pressedDown(){}

    protected open fun released(){}
}