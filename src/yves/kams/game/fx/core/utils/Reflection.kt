package yves.kams.game.fx.core.utils

class Reflection {
    companion object{
        fun <T> newInstance(type: Class<T>): T {
            try {
                return type.getConstructor().newInstance()
            } catch (e: Exception) {
                throw IllegalArgumentException(e.message + " for Class " + type.simpleName + " because the constructor is not the default constructor.")
            }
        }
    }
}