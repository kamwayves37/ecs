package yves.kams.game.fx.core.cache

class Cache{
    private val actionAdd: MutableList<Runnable> = mutableListOf()
    private val actionRemove: MutableList<Runnable> = mutableListOf()

    operator fun set(action: Runnable, type: CacheType){
        when(type){
            CacheType.ADDED -> actionAdd.add(action)
            CacheType.REMOVED -> actionRemove.add(action)
        }
    }

    fun onUpdate(){

        if (actionAdd.isNotEmpty()){
            println("Action Added")

            for (action in actionAdd) {
                action.run()
            }

            actionAdd.clear()
        }


        if(actionRemove.isNotEmpty()){
            println("Action Removed")

            for (action in actionRemove) {
                action.run()
            }

            actionRemove.clear()
        }
    }
}

enum class CacheType{
    ADDED,
    REMOVED
}
