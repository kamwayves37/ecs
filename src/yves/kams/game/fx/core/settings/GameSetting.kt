package yves.kams.game.fx.core.settings

import javafx.scene.Scene
import javafx.scene.canvas.GraphicsContext
import yves.kams.game.fx.core.cache.Cache

class GameSetting {
    var width: Double = 600.0

    var height: Double = 400.0

    var title: String = "GFX"

    var version: String = "0.1"

    var resizable: Boolean = false

    lateinit var graphicsContext: GraphicsContext

    lateinit var scene: Scene


    private val cache: Cache = Cache()

    fun readOnlyGameSettings(): ReadOnlyGameSetting {
        return ReadOnlyGameSetting(
                width,
                height,
                title,
                version,
                resizable,
                graphicsContext,
                scene,
                cache
        )
    }
}