package yves.kams.game.fx.core.game

import javafx.application.Application

import javafx.stage.Stage
import yves.kams.game.fx.core.engine.GameEngine
import yves.kams.game.fx.core.settings.GameSetting

abstract class GameApp : Application() {
    private lateinit var window: GameWindow
    private val engine = GameEngine()
    private val settings = GameSetting()

    override fun init() {
        initSettings(settings)
    }

    override fun start(state: Stage) {

        window = GameWindow(state, settings)

        state.show()

        engine.start()
    }

    override fun stop() {
        engine.stop()
    }

    protected abstract fun initSettings(settings: GameSetting)
}