package yves.kams.game.fx.core.game

import javafx.scene.Group
import javafx.scene.Scene
import javafx.scene.canvas.Canvas
import javafx.scene.layout.BorderPane
import javafx.stage.Stage
import yves.kams.game.fx.core.settings.GameSetting

class GameWindow(stage: Stage, settings: GameSetting){
    private val root = BorderPane()
    private val gameScene: Group = Group()
    private val scene: Scene = Scene(root)
    private val canvas = Canvas(settings.width, settings.height)

    init {
        gameScene.children += canvas

        root.left = gameScene

        stage.scene = scene

        settings.graphicsContext = canvas.graphicsContext2D
    }
}