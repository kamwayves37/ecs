package yves.kams.game.fx.core.engine

import yves.kams.game.fx.core.services.GameService

internal class GameEngine {
    private val services: MutableList<GameService> = mutableListOf()
    private val loop: GameLoop = GameLoop {
        update(it)
    }

    fun getTps(): Double{
        return loop.tps
    }

    fun getTpf(): Double{
        return loop.fps
    }

    private fun update(tps: Double) {
        //println("TPS: $tps" )
        services.forEach { it.onUpdate(tps) }
    }

    fun start() {
        services.forEach { it.onInit() }

        loop.start()
    }

    fun pause() {
        loop.pause()
    }

    fun resume() {
        loop.resume()
    }

    fun stop() {
        services.forEach { it.onExit() }

        loop.stop()
    }
}