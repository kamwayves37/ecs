package yves.kams.game.fx.core.engine

import javafx.animation.AnimationTimer
import java.util.*

internal class GameLoop(private val runnable: (Double) -> Unit) {

    private val fpsCounter = FPSCounter()
    private var isStarted: Boolean = false

    var fps: Double = 0.0
        private set

    var tps: Double = 0.0
        private set

    private val gameThread by lazy {

        object : AnimationTimer() {
            override fun handle(now: Long) {
                tps = tpfCompute(now)

                runnable(tps)
            }
        }
    }

    fun start() {
        if (!isStarted) {
            isStarted = true
            gameThread.start()
        }
    }

    fun stop() {
        gameThread.stop()
    }

    fun pause() {
        gameThread.stop()
        fpsCounter.reset()
    }

    fun resume() {
        gameThread.stop()
    }

    private fun tpfCompute(now: Long): Double {
        this.fps = fpsCounter.update(now).toDouble()
        if (this.fps < 5 || this.fps > 55) {
            this.fps = 60.0
        }
        return 1.0 / this.fps
    }

    private class FPSCounter {
        private val frameTimes = LongArray(MAX_SAMPLE)
        private var index = 0
        private var arrayFilled = false
        private var frameRate = 0

        fun update(now: Long): Int {
            val oldFrameTime = frameTimes[index]
            frameTimes[index] = now
            index = (index + 1) % frameTimes.size
            if (index == 0) {
                arrayFilled = true
            }
            if (arrayFilled) {
                val elapsedNanos = now - oldFrameTime
                val elapsedNanosPerFrame = elapsedNanos / frameTimes.size
                frameRate = (1000000000.0 / elapsedNanosPerFrame).toInt()
            }
            return frameRate
        }

        fun reset() {
            Arrays.fill(frameTimes, 0L)
            index = 0
            arrayFilled = false
            frameRate = 0
        }

        companion object {
            private const val MAX_SAMPLE = 100
        }
    }
}