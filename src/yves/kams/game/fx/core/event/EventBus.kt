package yves.kams.game.fx.core.event

import javafx.event.Event
import javafx.event.EventHandler
import javafx.event.EventType
import javafx.scene.Group

enum class EventBus {
    INSTANCE;

    private val group: Group = Group()

    fun fireEvent(event: Event){
        group.fireEvent(event)
    }

    fun <T : Event> addEventHandler(type: EventType<out T>, handler: EventHandler<in T>): Subscribe{
        group.addEventHandler(type, handler)

        @Suppress("UNCHECKED_CAST")
        return Subscribe(type, handler as EventHandler<in Event>)
    }

    fun <T : Event> removeEventHandler(type: EventType<out T>, handler: EventHandler<in T>){
        group.removeEventHandler(type, handler)
    }
}