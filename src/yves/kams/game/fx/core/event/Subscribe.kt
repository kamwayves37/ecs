package yves.kams.game.fx.core.event

import javafx.event.Event
import javafx.event.EventHandler
import javafx.event.EventType

class Subscribe(private val type: EventType<out Event>,
                private val handler: EventHandler<in Event>) {

    fun unSubscribe(){
        EventBus.INSTANCE.removeEventHandler(type, handler)
    }
}