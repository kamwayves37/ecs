package yves.kams.game.fx.core.services

abstract class GameService {
    open fun onInit(){}

    open fun onUpdate(dt: Double){}

    open fun onExit(){}
}