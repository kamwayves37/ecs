package yves.kams.game.fx.app.systems

import javafx.scene.input.KeyCode
import yves.kams.game.fx.app.GameType
import yves.kams.game.fx.app.components.InputStateComponent
import yves.kams.game.fx.app.components.KeyboardStateComponent
import yves.kams.game.fx.app.components.MotionComponent
import yves.kams.game.fx.core.ecs.systems.System

class PlayerKeyboardControls : System() {

    override fun onInit() {
        query.key = listOf(
                KeyboardStateComponent::class,
                InputStateComponent::class,
                MotionComponent::class
        )
    }

    override fun onUpdate(dt: Double) {
        query.results.forEach { entity ->
            val keyboard = entity.getComponent(KeyboardStateComponent::class)
            val inputState = entity.getComponent(InputStateComponent::class)
            val motion = entity.getComponent(MotionComponent::class)

            when (entity.type) {

                GameType.PLAYER_ONE -> {
                    //Changed
                    if (inputState.anyChanged()) {
                        if (keyboard.isPressedDown(KeyCode.UP)) motion.y = -80.0
                        if (keyboard.isPressedDown(KeyCode.DOWN)) motion.y = 80.0
                        if (keyboard.isPressedDown(KeyCode.LEFT)) motion.x = -80.0
                        if (keyboard.isPressedDown(KeyCode.RIGHT)) motion.x = 80.0
                    }

                    //Released
                    if (inputState.anyReleased()) {
                        if (keyboard.isReleased(KeyCode.UP)) motion.y = 0.0
                        if (keyboard.isReleased(KeyCode.DOWN)) motion.y = 0.0
                        if (keyboard.isReleased(KeyCode.LEFT)) motion.x = 0.0
                        if (keyboard.isReleased(KeyCode.RIGHT)) motion.x = 0.0
                    }
                }

                GameType.PLAYER_TWO -> {

                    with(inputState){
                        if(anyChanged()){
                            //Changed
                            if (keyboard.isPressedDown(KeyCode.Z)) motion.y = -80.0
                            if (keyboard.isPressedDown(KeyCode.S)) motion.y = 80.0
                            if (keyboard.isPressedDown(KeyCode.Q)) motion.x = -80.0
                            if (keyboard.isPressedDown(KeyCode.D)) motion.x = 80.0
                        }

                        //Released
                        if(anyReleased()){
                            if (keyboard.isReleased(KeyCode.Z)) motion.y = 0.0
                            if (keyboard.isReleased(KeyCode.S)) motion.y = 0.0
                            if (keyboard.isReleased(KeyCode.Q)) motion.x = 0.0
                            if (keyboard.isReleased(KeyCode.D)) motion.x = 0.0
                        }
                    }
                }
            }
        }
    }

    override fun onExit() {
    }
}