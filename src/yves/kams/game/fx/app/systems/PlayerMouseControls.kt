package yves.kams.game.fx.app.systems

import javafx.scene.input.MouseButton
import yves.kams.game.fx.app.components.InputStateComponent
import yves.kams.game.fx.app.components.TransformComponent
import yves.kams.game.fx.core.ecs.systems.System

class PlayerMouseControls: System() {
    override fun onInit() {
        query.key = listOf(
                InputStateComponent::class,
                TransformComponent::class
        )
    }

    override fun onUpdate(dt: Double) {

    }

    override fun onExit() {
    }
}