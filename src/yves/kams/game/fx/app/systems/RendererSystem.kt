package yves.kams.game.fx.app.systems

import javafx.scene.canvas.GraphicsContext
import javafx.scene.paint.Color
import yves.kams.game.fx.app.GameType
import yves.kams.game.fx.app.components.ShapeComponent
import yves.kams.game.fx.app.components.TransformComponent
import yves.kams.game.fx.core.ecs.systems.System

class RendererSystem : System() {
    private lateinit var gc: GraphicsContext
    private var canvasWidth: Double = 0.0
    private var canvasHeight: Double = 0.0

    override fun onInit() {
        query.key = listOf(TransformComponent::class, ShapeComponent::class)

        val setting = world.setting

        gc = setting.graphicsContext
        canvasWidth = setting.width
        canvasHeight = setting.height
    }

    override fun onUpdate(dt: Double) {
        gc.clearRect(0.0, 0.0, canvasWidth, canvasHeight)

        query.results.forEach { entity ->

            val transform = entity.getComponent(TransformComponent::class)
            val shape = entity.getComponent(ShapeComponent::class)

            gc.fill = shape.color
            gc.fillRect(transform.x, transform.y, shape.w, shape.h)
        }
    }

    override fun onExit() {
        query.reset()
    }
}