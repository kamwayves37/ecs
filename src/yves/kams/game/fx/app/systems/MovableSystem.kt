package yves.kams.game.fx.app.systems

import yves.kams.game.fx.app.components.MotionComponent
import yves.kams.game.fx.app.components.ShapeComponent
import yves.kams.game.fx.app.components.TransformComponent
import yves.kams.game.fx.core.ecs.systems.System

class MovableSystem : System() {
    private var canvasWidth: Double = 0.0
    private var canvasHeight: Double = 0.0

    override fun onInit() {
        query.key = listOf(MotionComponent::class, TransformComponent::class)

        val setting = world.setting

        canvasWidth = setting.width
        canvasHeight = setting.height
    }

    override fun onUpdate(dt: Double) {
        query.results.forEach {entity ->
            val motion = entity.getComponent(MotionComponent::class)
            val transform = entity.getComponent(TransformComponent::class)
            val shape = entity.getComponent(ShapeComponent::class)

            transform.x += motion.x * dt
            transform.y += motion.y * dt

            if(transform.x < -shape.w) transform.x = canvasWidth + 15.0
            if(transform.x  > canvasWidth + 15.0) transform.x = -15.0
            if(transform.y < -15.0) transform.y = canvasHeight + 15.0
            if(transform.y  > canvasHeight + 15.0) transform.y = -15.0
        }
    }

    override fun onExit() {
        query.reset()
    }
}