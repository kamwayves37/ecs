package yves.kams.game.fx.app.components

import yves.kams.game.fx.core.ecs.components.Component

class InputStateComponent : Component(){
    var pressed: Boolean = false
    var changed: Boolean = false
    var released: Boolean = false

    fun reset(){
        pressed = false
        changed = false
        released = false
    }

    fun anyPressed(): Boolean{
        return pressed
    }

    fun anyChanged(): Boolean{
        return changed
    }

    fun anyReleased(): Boolean{
        return released
    }
}