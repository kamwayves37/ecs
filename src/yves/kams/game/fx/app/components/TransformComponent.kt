package yves.kams.game.fx.app.components

import yves.kams.game.fx.core.ecs.components.Component

data class TransformComponent(var x: Double, var y: Double): Component()