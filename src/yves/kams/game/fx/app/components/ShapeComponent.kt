package yves.kams.game.fx.app.components

import javafx.scene.paint.Color
import yves.kams.game.fx.core.ecs.components.Component

data class ShapeComponent(val primitive: String = "box", val color: Color = Color.BLACK, val w: Double = 15.0, val h: Double = 15.0): Component()