package yves.kams.game.fx.app.components

import javafx.scene.input.KeyCode
import yves.kams.game.fx.core.ecs.components.Component
import yves.kams.game.fx.core.input.InputButton

class KeyboardStateComponent(val mapping: Array<KeyCode>) : Component(){
    private val states: MutableMap<KeyCode, InputButton> = mutableMapOf()

    operator fun get(key: KeyCode): InputButton {
        var inputButton = states[key]
        if(inputButton === null){
            inputButton = InputButton()
            states[key] = inputButton
        }
        return inputButton
    }

    fun isPressed(key: KeyCode): Boolean{
        return states[key]?.keyDown ?: false
    }

    fun isPressedDown(key: KeyCode): Boolean{
        return states[key]?.keyPressed ?: false
    }

    fun isReleased(key: KeyCode): Boolean{
        return states[key]?.keyUp ?: false
    }
}