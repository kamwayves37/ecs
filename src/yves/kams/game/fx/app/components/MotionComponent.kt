package yves.kams.game.fx.app.components

import yves.kams.game.fx.core.ecs.components.Component

data class MotionComponent(var x: Double = 0.0, var y: Double = 0.0) : Component()