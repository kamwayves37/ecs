package yves.kams.game.fx.app

enum class GameType {
    PLAYER_ONE,
    PLAYER_TWO,
    PLAYER_THREE,
    ENEMY
}